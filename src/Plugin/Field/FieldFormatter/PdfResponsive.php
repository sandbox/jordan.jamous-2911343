<?php
/**
 * Created by PhpStorm.
 * User: amin
 * Date: 20/09/17
 * Time: 2:34 PM
 */

namespace Drupal\unoconv\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldFormatter(
 *  id = "pdf_unoconv_responsive",
 *  label = @Translation("PDF: Unoconv Responsive viewer of PDF.js"),
 *  description = @Translation("Use the default viewer like
 *   http://mozilla.github.io/pdf.js/web/viewer.html."), field_types = {"file"}
 * )
 */
class PdfResponsive extends FormatterBase {

  public static function defaultSettings() {
    return [
        'keep_pdfjs' => TRUE,
        'width' => '100%',
        'height' => '',
      ] + parent::defaultSettings();
  }

  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['keep_pdfjs'] = [
      '#type' => 'checkbox',
      '#title' => t('Always use pdf.js'),
      '#default_value' => $this->getSetting('keep_pdfjs'),
      '#description' => t("Use pdf.js even the browser has Adobe Reader Plugin, WebKit PDF Reader for Safari or the PDF Reader for Chrome (Chrome's default alternative to the Adobe Reader Plugin) installed."),
    ];

    $elements['width'] = [
      '#type' => 'textfield',
      '#title' => 'Width',
      '#default_value' => $this->getSetting('width'),
      '#description' => t('Width of the viewer. Ex: 250px or 100%'),
    ];

    $elements['height'] = [
      '#type' => 'textfield',
      '#title' => 'Height',
      '#default_value' => $this->getSetting('height'),
      '#description' => t('Height of the viewer. Ex: 250px or 100%'),
    ];
    return $elements;
  }

  public function settingsSummary() {
    $summary = [];

    $keep_pdfjs = $this->getSetting('keep_pdfjs');
    $width = $this->getSetting('width');
    $height = $this->getSetting('height');
    if (empty($keep_pdfjs) && empty($width) && empty($height)) {
      $summary[] = $this->t('No settings');
    }
    else {
      $summary[] = t('Use pdf.js even users have PDF reader plugin: @keep_pdfjs', ['@keep_pdfjs' => $keep_pdfjs ? t('Yes') : t('No')]) . '. ' . t('Widht: @width , Height: @height', [
          '@width' => $width,
          '@height' => $height,
        ]);
    }

    return $summary;
  }

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $html = [
      '#prefix' => '<div class="responsive-pdf">',
      '#suffix' => '</div>',
      '#theme' => 'unoconv',
      '#file' => [], // $item->entity
      '#attributes' => [
        'class' => ['pdf'],
        'webkitallowfullscreen' => '',
        'mozallowfullscreen' => '',
        'allowfullscreen' => '',
        'frameborder' => 'no',
        'width' => $this->getSetting('width'),
        'height' => $this->getSetting('height'),
        'src' => '',//$iframe_src
        'data-src' => '', //$file_url,
      ],
    ];
    $elements = [];
    foreach ($items as $delta => $item) {
      switch ($item->entity->getMimeType()) {
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
        case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
        case 'application/msword':
        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
        case 'application/vnd.ms-powerpoint':
        case 'text/csv':
        case 'text/plain':
        case 'application/vnd.ms-excel':

          //did we save a pdf copy for this file?
          $unoconv_file = NULL;
          if ($ufid = self::unoconv_get_ufid($item->entity)) {
            //yes there is a copy for this file so just view it
            $unoconv_file = \Drupal::entityTypeManager()
              ->getStorage('file')
              ->load($ufid);
          }
          else {
            // convert into a pdf file
            if ($ufile = $this->convert_and_save_to_database($item->entity)) {
              $unoconv_file = $ufile;
            }
          }

          $file_url = file_create_url($unoconv_file->getFileUri());
          $iframe_src = file_create_url('/libraries/pdf.js/web/viewer.html') . '?file=' . rawurlencode($file_url);
          $html['#file'] = $unoconv_file;
          $html['#attributes']['src'] = $iframe_src;
          $html['#attributes']['data-src'] = $file_url;
          $elements[$delta] = $html;
          break;
        case 'application/pdf':
          $file_url = file_create_url($item->entity->getFileUri());
          $iframe_src = file_create_url('/libraries/pdf.js/web/viewer.html') . '?file=' . rawurlencode($file_url);
          $html['#file'] = $item->entity;
          $html['#attributes']['src'] = $iframe_src;
          $html['#attributes']['data-src'] = $file_url;
          $elements[$delta] = $html;
          break;
        default:
          $elements[$delta] = [
            '#theme' => 'file_link',
            '#file' => $item->entity,
          ];
      }
    }
    if ($this->getSetting('keep_pdfjs') != TRUE) {
      $elements['#attached']['library'][] = 'pdf/default';
    }
    return $elements;
  }

  public static function unoconv_get_ufid($entity) {
    $fid = $entity->id();
    $result = \Drupal::database()->select('file_usage', 'fu')
      ->fields('fu', ['fid'])
      ->condition('id', $fid, '=')
      ->condition('module', 'unoconv', '=')
      ->condition('type', 'file', '=')
      ->execute()->fetchAll(\PDO::FETCH_ASSOC);
    if (isset($result[0]['fid'])) {
      return $result[0]['fid'];
    }
    return FALSE;
  }

  private function convert_and_save_to_database(\Drupal\file\Entity\File $file_entity) {

    $params = [];
    $unoconv = \Drupal\Core\Site\Settings::get('convert_file_format_url');
    if (!$unoconv) {
      \Drupal::logger('unoconv')
        ->error('unoconv', 'PDF document could not be generated because "Unoconv server url" is not configured. <a href="/admin/config/media/unoconv">configure?</a>');
      return FALSE;
    }

    $params['file'] = curl_file_create(\Drupal::service('file_system')
      ->realpath($file_entity->getFileUri()));
    $request = curl_init();
    curl_setopt($request, CURLOPT_URL, $unoconv);
    curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($request, CURLOPT_POST, 1);
    curl_setopt($request, CURLOPT_POSTFIELDS, $params);
    $data = curl_exec($request);
    curl_close($request);
    $file_name = $file_entity->getFilename() . ".pdf";
    if ($data) {
      // now get Drupal to save it
      $ufile = file_save_data($data, 'private://' . $file_name);
      /** @var \Drupal\file\FileUsage\FileUsageInterface $file_usage */
      $file_usage = \Drupal::service('file.usage');
      $file_usage->add($ufile, 'unoconv', 'file', $file_entity->id());

      \Drupal::logger('unoconv')
        ->info('A new PDF file has been created and saved to DB fid[' . $ufile->id() . ']');
      return $ufile;
    }
    return NULL;
  }

}