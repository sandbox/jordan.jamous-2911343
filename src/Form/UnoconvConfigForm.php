<?php
/**
 * Created by PhpStorm.
 * User: amin
 * Date: 20/09/17
 * Time: 3:41 PM
 */

namespace Drupal\unoconv\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;

class UnoconvConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unoconv_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('unoconv.settings');

    $form['convert_file_format_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unoconv server url'),
      '#default_value' => Settings::get('convert_file_format_url') == NULL ? '' : Settings::get('convert_file_format_url'),
      '#description' => $this->t('Enter Uniconv service URL. ie: http://unoconv:3000/unoconv/pdf , for more information refer to <a href="https://github.com/zrrrzzt/docker-unoconv-webservice">Docker Unoconv Webservice</a>'),
      '#size' => 170,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('unoconv.settings')
      ->set('element', $form_state->getValue('convert_file_format_url'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unoconv.settings'];
  }

}